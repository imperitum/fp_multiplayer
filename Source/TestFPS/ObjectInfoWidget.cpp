// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectInfoWidget.h"
#include "Components/TextBlock.h"
#include "Components/WidgetSwitcher.h"

void UObjectInfoWidget::SetInfo(const FText& Name, bool bIsConsumable, const FText& ObjectDescription, bool bIsHeal, float ValueToAdd)
{
	NameTXT->SetText(Name);

	if (bIsConsumable) 
	{
		IsConsumableSwitcher->SetActiveWidgetIndex(1);
		ValueToAddTXT->SetText(FText::AsNumber(ValueToAdd));

		if (bIsHeal)
		{
			ValueToAddTXT->SetColorAndOpacity(FLinearColor::Green);
		}
		else
		{
			ValueToAddTXT->SetColorAndOpacity(FLinearColor::Blue);
		}
	}
	else
	{
		DescriptionTXT->SetText(ObjectDescription);
	}
}

void UObjectInfoWidget::SetShowNameOnly(const FText& Name)
{
	NameTXT->SetText(Name);
	IsConsumableSwitcher->SetVisibility(ESlateVisibility::Collapsed);
}
