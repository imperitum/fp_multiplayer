// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ObjectInfoWidget.generated.h"

/**
 * 
 */
UCLASS()
class TESTFPS_API UObjectInfoWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* NameTXT;

	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* IsConsumableSwitcher;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* DescriptionTXT;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ValueToAddTXT;

public:

	void SetInfo(const FText& Name, bool bIsConsumable, const FText& ObjectDescription, bool bIsHeal, float ValueToAdd);

	void SetShowNameOnly(const FText& Name);
	
};
