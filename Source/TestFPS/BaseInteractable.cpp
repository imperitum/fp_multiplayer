// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseInteractable.h"
#include "ObjectInfoWidget.h"
#include "TestFPSCharacter.h"
#include "Net/UnrealNetwork.h"
#include "CharacteristicsComponent.h"

// Sets default values
ABaseInteractable::ABaseInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = BaseMesh;

	ObjectInfoWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Popup Info"));
	ObjectInfoWidget->SetupAttachment(BaseMesh);
	ObjectInfoWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 20.0f));

	SetReplicates(true);
	SetReplicateMovement(true);
	NetUpdateFrequency = 3.f;
}

// Called when the game starts or when spawned
void ABaseInteractable::BeginPlay()
{
	Super::BeginPlay();

	ObjectInfoWidget->SetVisibility(false);
	InfoWidget = (UObjectInfoWidget*)(ObjectInfoWidget->GetUserWidgetObject());
	if (InfoWidget)
	{
		InfoWidget->SetInfo(ObjectName, bIsConsumable, ObjectDescription, bIsHeal, ValueToAdd);
	}
}

void ABaseInteractable::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ABaseInteractable, ValueToAdd, COND_InitialOnly);
}

void ABaseInteractable::StartFocus_Implementation()
{
	ObjectInfoWidget->SetVisibility(true);
}

void ABaseInteractable::EndFocus_Implementation()
{
	ObjectInfoWidget->SetVisibility(false);
}

void ABaseInteractable::OnInteract_Implementation(AActor* InteractingActor)
{
	if (bIsConsumable)
	{
		ATestFPSCharacter* ActorThatInteract = Cast<ATestFPSCharacter>(InteractingActor);
		if (ActorThatInteract)
		{
			if (bIsHeal)
			{
				ActorThatInteract->AddPlayerHealth(ValueToAdd, this);
			}
			else if (bIsFeed)
			{
				ActorThatInteract->AddPlayerSatiety(ValueToAdd, this);
			}
		}
	}
}