// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacteristicsComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTFPS_API UCharacteristicsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacteristicsComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void TakeDamage(AActor* DamageActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

public:	

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	float GetPlayerHealth() const { return Health; }
	float GetPlayerSatiety() const { return Satiety; }

	void AddHealth(float HealthToAdd);

	UFUNCTION(Server, reliable, WithValidation)
	void Server_AddHealth(float HealthToAdd);
	bool Server_AddHealth_Validate(float HealthToAdd);
	void Server_AddHealth_Implementation(float HealthToAdd);

	void AddSatiety(float SatietyToAdd);

	UFUNCTION(Server, reliable, WithValidation)
	void Server_AddSatiety(float SatietyToAdd);
	bool Server_AddSatiety_Validate(float SatietyToAdd);
	void Server_AddSatiety_Implementation(float SatietyToAdd);

private:

	FTimerHandle SatietyTimerHandle;

	UFUNCTION()
	void UpdateSatiety();

	UFUNCTION()
	void OnRep_Health();

	UFUNCTION(Server, reliable)
	void Server_KillPlayer();
	void Server_KillPlayer_Implementation();

	UFUNCTION(NetMulticast, reliable)
	void Client_KillPlayer();
	void Client_KillPlayer_Implementation();

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float DefaultHealth = 100.f;

	UPROPERTY(ReplicatedUsing = OnRep_Health, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float Health = 0.f;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float DefaultSatiety = 100.f;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float Satiety = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float SatietyRefreshRate = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float SatietyTickDamage = 10.f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageType> LowSatietyDamageType;
	
};
