// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestFPSCharacter.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "InteractInterface.h"
#include "ObjectInfoWidget.h"
#include "Net/UnrealNetwork.h"
#include "Components/WidgetComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

ATestFPSCharacter::ATestFPSCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetupAttachment(GetCapsuleComponent());

	ObjectInfoWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Popup Info"));
	ObjectInfoWidget->SetupAttachment(GetCapsuleComponent());

	Characteristics = CreateDefaultSubobject<UCharacteristicsComponent>(TEXT("Component"));
}

void ATestFPSCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATestFPSCharacter::OnInteract);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ATestFPSCharacter::OnAttack);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ATestFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATestFPSCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATestFPSCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATestFPSCharacter::LookUpAtRate);
}

void ATestFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = (APlayerController*)(Controller);

	ObjectInfoWidget->SetVisibility(false);
	InfoWidget = (UObjectInfoWidget*)(ObjectInfoWidget->GetUserWidgetObject());
	if (InfoWidget)
	{
		InfoWidget->SetShowNameOnly(FText::FromString(this->GetName()));
	}
}

void ATestFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckForInteraction();
}

void ATestFPSCharacter::CheckForInteraction()
{
	if (PlayerController)
	{
		FHitResult HitResult = GetPlayerViewHit();

		if (HitResult.bBlockingHit)
		{
			AActor* Interactable = HitResult.GetActor();
			if (Interactable)
			{
				if (Interactable != FocusedActor)
				{
					if (FocusedActor)
					{
						IInteractInterface* InteractInterface = Cast<IInteractInterface>(FocusedActor);
						if (InteractInterface) InteractInterface->Execute_EndFocus(FocusedActor);
					}
					IInteractInterface* InteractInterface = Cast<IInteractInterface>(Interactable);
					if (InteractInterface) InteractInterface->Execute_StartFocus(Interactable);
					FocusedActor = Interactable;
				}
			}
			else if (FocusedActor)
			{
				IInteractInterface* InteractInterface = Cast<IInteractInterface>(FocusedActor);
				if (InteractInterface) InteractInterface->Execute_EndFocus(FocusedActor);
				FocusedActor = nullptr;
			}
		}
		else if (FocusedActor)
		{
			IInteractInterface* InteractInterface = Cast<IInteractInterface>(FocusedActor);
			if (InteractInterface) InteractInterface->Execute_EndFocus(FocusedActor);
			FocusedActor = nullptr;
		}
	}
}

FHitResult ATestFPSCharacter::GetPlayerViewHit() const
{
	FHitResult Hit;
	FRotator Rotation;
	FVector StartLocation;

	PlayerController->GetPlayerViewPoint(StartLocation, Rotation);

	FVector EndLocation = StartLocation + (Rotation.Vector() * 200.f);

	GetWorld()->SweepSingleByChannel(
		Hit,
		StartLocation,
		EndLocation,
		{ 0.f, 0.f, 0.f, 0.f },
		ECollisionChannel::ECC_Visibility,
		FCollisionShape::MakeSphere(30.f),
		FCollisionQueryParams(FName(TEXT("")), true, this)
	);

	return Hit;
}

UCharacteristicsComponent* ATestFPSCharacter::GetCharacteristicsComponent() const
{
	return Characteristics;
}

void ATestFPSCharacter::Die()
{
	if (PlayerController)
	{
		PlayerController->UnPossess();
	}
	Mesh1P->SetSimulatePhysics(true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ATestFPSCharacter::StartFocus_Implementation()
{
	ObjectInfoWidget->SetVisibility(true);
}

void ATestFPSCharacter::EndFocus_Implementation()
{
	ObjectInfoWidget->SetVisibility(false);
}

void ATestFPSCharacter::AddPlayerHealth(float HealthToAdd, AActor* CauserActor)
{
	Characteristics->AddHealth(HealthToAdd);
	Server_DestroyActor(CauserActor);
}

void ATestFPSCharacter::AddPlayerSatiety(float SatietyToAdd, AActor* CauserActor)
{
	Characteristics->AddSatiety(SatietyToAdd);
	Server_DestroyActor(CauserActor);
}

void ATestFPSCharacter::OnInteract()
{
	FHitResult HitResult = GetPlayerViewHit();
	AActor* Interactable = HitResult.GetActor();
	if (Interactable)
	{
		IInteractInterface* InteractInterface = Cast<IInteractInterface>(Interactable);
		if (InteractInterface) InteractInterface->Execute_OnInteract(Interactable, this);
	}
}

void ATestFPSCharacter::OnAttack()
{
	if (bIsAttackAllowed)
	{
		Server_PlayAttackAnim(AttackAnimation);

		FHitResult Hit;
		FRotator Rotation;
		FVector StartLocation;

		PlayerController->GetPlayerViewPoint(StartLocation, Rotation);

		FVector EndLocation = StartLocation + (Rotation.Vector() * 100.f);

		GetWorld()->SweepSingleByChannel(
			Hit,
			StartLocation,
			EndLocation,
			{ 0.f, 0.f, 0.f, 0.f },
			ECollisionChannel::ECC_Pawn,
			FCollisionShape::MakeSphere(80.f),
			FCollisionQueryParams(FName(TEXT("")), true, this)
		);

		if (Hit.bBlockingHit && Hit.GetActor())
		{
			Server_DamageActor(Hit.GetActor());
		}

		bIsAttackAllowed = false;
		GetWorld()->GetTimerManager().SetTimer(AttackRefreshingHandle, this, &ATestFPSCharacter::AllowAttack, 1.f, false);
	}
}

void ATestFPSCharacter::Server_PlayAttackAnim_Implementation(UAnimMontage* MontageToPlay)
{
	Client_PlayAttackAnim(MontageToPlay);
}

void ATestFPSCharacter::Client_PlayAttackAnim_Implementation(UAnimMontage* MontageToPlay)
{
	if (MontageToPlay != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(MontageToPlay, 1.f);
		}
	}
}

void ATestFPSCharacter::AllowAttack() 
{ 
	bIsAttackAllowed = true;
}

void ATestFPSCharacter::Server_DamageActor_Implementation(AActor* ActorToDamage)
{
	UGameplayStatics::ApplyDamage(ActorToDamage, 10.f, PlayerController, this, DamageType);
}

void ATestFPSCharacter::Server_DestroyActor_Implementation(AActor* ActorToDestroy)
{
	if (IsValid(ActorToDestroy))
	{
		ActorToDestroy->Destroy();
	}
}

void ATestFPSCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATestFPSCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATestFPSCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATestFPSCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}