// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractInterface.h"
#include "Components/WidgetComponent.h"
#include "BaseInteractable.generated.h"

UCLASS()
class TESTFPS_API ABaseInteractable : public AActor, public IInteractInterface
{
	GENERATED_BODY()

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BaseMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* ObjectInfoWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	FText ObjectName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	bool bIsConsumable = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	FText ObjectDescription;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	bool bIsHeal = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	bool bIsFeed = false;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Object Info", meta = (AllowPrivateAccess = "true"))
	float ValueToAdd = 35.f;

	class UObjectInfoWidget* InfoWidget = nullptr;

public:
	// Sets default values for this actor's properties
	ABaseInteractable();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	virtual void StartFocus_Implementation() override;
	virtual void EndFocus_Implementation() override;
	virtual void OnInteract_Implementation(AActor* InteractingActor) override;
};
