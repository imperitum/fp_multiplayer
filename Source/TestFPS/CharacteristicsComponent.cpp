// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacteristicsComponent.h"
#include "TestFPSCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UCharacteristicsComponent::UCharacteristicsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UCharacteristicsComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = DefaultHealth;
	Satiety = DefaultSatiety;

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UCharacteristicsComponent::TakeDamage);

	GetWorld()->GetTimerManager().SetTimer(SatietyTimerHandle, this, &UCharacteristicsComponent::UpdateSatiety, SatietyRefreshRate, true);
}

void UCharacteristicsComponent::TakeDamage(AActor* DamageActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage != 0)
	{
		Health = FMath::Clamp(Health - Damage, 0.f, DefaultHealth);
	}
}

void UCharacteristicsComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UCharacteristicsComponent, DefaultHealth, COND_InitialOnly);
	DOREPLIFETIME(UCharacteristicsComponent, Health);
	DOREPLIFETIME_CONDITION(UCharacteristicsComponent, DefaultSatiety, COND_InitialOnly);
	DOREPLIFETIME(UCharacteristicsComponent, Satiety);
}

void UCharacteristicsComponent::AddHealth(float HealthToAdd)
{
	Server_AddHealth(HealthToAdd);
}

void UCharacteristicsComponent::AddSatiety(float SatietyToAdd)
{
	Server_AddSatiety(SatietyToAdd);
}

bool UCharacteristicsComponent::Server_AddHealth_Validate(float HealthToAdd)
{
	if (HealthToAdd > DefaultHealth) return false;
	return true;
}

void UCharacteristicsComponent::Server_AddHealth_Implementation(float HealthToAdd)
{
	Health = FMath::Clamp(Health + HealthToAdd, 0.f, DefaultHealth);
}

bool UCharacteristicsComponent::Server_AddSatiety_Validate(float SatietyToAdd)
{
	if (SatietyToAdd > DefaultSatiety) return false;
	return true;
}

void UCharacteristicsComponent::Server_AddSatiety_Implementation(float SatietyToAdd)
{
	Satiety = FMath::Clamp(Satiety + SatietyToAdd, 0.f, DefaultSatiety);
}

void UCharacteristicsComponent::UpdateSatiety()
{
	if (Satiety <= 0.f)
	{
		UGameplayStatics::ApplyDamage(GetOwner(), SatietyTickDamage, GetOwner()->GetInstigatorController(), GetOwner(), LowSatietyDamageType);
	}
	else
	{
		Satiety = FMath::Clamp(Satiety - SatietyTickDamage, 0.f, DefaultSatiety);
	}
}

void UCharacteristicsComponent::OnRep_Health()
{
	Server_KillPlayer();
}

void UCharacteristicsComponent::Server_KillPlayer_Implementation()
{
	Client_KillPlayer();
}

void UCharacteristicsComponent::Client_KillPlayer_Implementation()
{
	if (Health <= 0.f)
	{
		ATestFPSCharacter* PlayerChar = Cast<ATestFPSCharacter>(GetOwner());
		PlayerChar->Die();
	}
}
