// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InteractInterface.h"
#include "CharacteristicsComponent.h"
#include "TestFPSCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

UCLASS(config=Game)
class ATestFPSCharacter : public ACharacter, public IInteractInterface
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCharacteristicsComponent* Characteristics;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* ObjectInfoWidget;

public:

	ATestFPSCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual UCharacteristicsComponent* GetCharacteristicsComponent() const;
	void Die();

	virtual void StartFocus_Implementation() override;
	virtual void EndFocus_Implementation() override;

protected:
	virtual void BeginPlay();

public:

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* AttackAnimation;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageType> DamageType;

	void AddPlayerHealth(float HealthToAdd, AActor* CauserActor);

	void AddPlayerSatiety(float SatietyToAdd, AActor* CauserActor);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	float GetPlayerHealth() { return Characteristics->GetPlayerHealth(); }

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	float GetPlayerSatiety() { return Characteristics->GetPlayerSatiety(); }

protected:
	
	void OnInteract();

	void OnAttack();

	void OnAttackFinished();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

private:

	APlayerController* PlayerController = nullptr;

	AActor* FocusedActor = nullptr;

	class UObjectInfoWidget* InfoWidget = nullptr;

	bool bIsAttackAllowed = true;

	FHitResult GetPlayerViewHit() const;

	FTimerHandle AttackRefreshingHandle;

	UFUNCTION()
	void AllowAttack();

	UFUNCTION()
	void CheckForInteraction();

	UFUNCTION(Server, reliable)
	void Server_DestroyActor(AActor* ActorToDestroy);

	UFUNCTION(Server, reliable)
	void Server_DamageActor(AActor* ActorToDestroy);

	UFUNCTION(Server, unreliable)
	void Server_PlayAttackAnim(UAnimMontage* MontageToPlay);

	UFUNCTION(NetMulticast, unreliable)
	void Client_PlayAttackAnim(UAnimMontage* MontageToPlay);
};

