// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractInterface.h"

// Add default functionality here for any IInteractInterface functions that are not pure virtual.

void IInteractInterface::StartFocus_Implementation()
{
}

void IInteractInterface::EndFocus_Implementation()
{
}

void IInteractInterface::OnInteract_Implementation(AActor* InteractingActor)
{
}
